# Mainsite-Node
This repository holds the Node.js source code for my personal/professional site at [benhigh.xyz](http://www.benhigh.xyz/).

## Stack
- Node.js
- Express
- Helmet (for security)
- Pug/Jade
- SASS

## Quick note about copyright + trademark
This repository contains images for certain technologies and companies. They are not owned by me, and are being used
within the appropriate usage guidelines, issued by their respective companies.

The Spring logo is a registered trademark of Pivotal Software, Inc. The Android logo is a registered trademark of
Google, LLC. The Twitter logo is a registered trademark of Twitter Inc. The GitLab logo is a registered trademark of
GitLab, Inc. The Unity logo is a registered trademark of Unity Technologies.

The mail image at the bottom of the page is licensed under CC0 and comes from
[here](https://www.iconsdb.com/white-icons/email-icon.html). The image of my face is my own, and is licensed under CC0.

Except where otherwise indicated, all code resources are licensed under the MIT license.